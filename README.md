# Setup Instructions

### Clone this repo in current directory

**Create and switch to a new empty directory and run following command**

```
git clone https://gitlab.com/sagarpanchal/cra-react-redux.git .
```

**Install all dependencies**

```
npm i   # install required npm packages
```

**Run the project**

```
npm start   # for live updates,
npm build   # for development build.
```

### OPTIONAL

**ESLint plugins for vscode**

```
npm i -g eslint eslint-plugin-react babel-eslint
```

**Remove existing git repo using powershell on windows**

```
Remove-Item -Recurse -Force .\.git\
```
