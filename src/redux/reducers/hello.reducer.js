export const initState = {
  text: '',
}

export const helloActionTypes = {
  SAY_HELLO: '@hello/say-hello',
  SAY_HELLO_ASYNC: '@saga/@hello/say-hello-async',
  RESET: '@hello/reset',
}

const hello = (state = initState, action) => {
  switch (action.type) {
    case helloActionTypes.SAY_HELLO:
      return { ...state, ...action.data }

    case helloActionTypes.RESET:
      return { ...initState }

    default:
      return state
  }
}

export default hello
