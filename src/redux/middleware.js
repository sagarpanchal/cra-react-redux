import createSagaMiddleware from 'redux-saga'

export const saga = createSagaMiddleware()

export default [saga]
