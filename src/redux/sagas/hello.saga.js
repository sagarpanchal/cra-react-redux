import { put, takeEvery } from 'redux-saga/effects'
import { helloActionTypes } from 'redux/reducers/hello.reducer'
import HelloActions from 'redux/actions/hello.actions'

export function* sayHelloAsync(action) {
  yield new Promise((r) => setTimeout(r, 2000))
  yield put(HelloActions.sayHello(`Hello, ${action?.data?.text}!`))
}

export function* watchHello() {
  yield takeEvery(helloActionTypes.SAY_HELLO_ASYNC, sayHelloAsync)
}
