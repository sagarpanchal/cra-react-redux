import { all } from 'redux-saga/effects'
import { watchHello } from './hello.saga'

export function* rootSaga() {
  yield all([watchHello()])
}
