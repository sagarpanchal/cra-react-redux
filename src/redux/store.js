import { combineReducers, createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import reducers from 'redux/reducers'
import middleware from 'redux/middleware'

export const store = createStore(
  combineReducers({ ...reducers }),
  composeWithDevTools({ trace: true })(applyMiddleware(...middleware))
)
