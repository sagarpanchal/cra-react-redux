import { helloActionTypes } from 'redux/reducers/hello.reducer'

const HelloActions = {}

HelloActions.sayHello = (text) => ({
  type: helloActionTypes.SAY_HELLO,
  data: { text },
})

HelloActions.sayHelloAsync = (text) => ({
  type: helloActionTypes.SAY_HELLO_ASYNC,
  data: { text },
})

HelloActions.reset = () => ({
  type: helloActionTypes.RESET,
})

export default HelloActions
