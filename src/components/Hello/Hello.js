import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import HelloActions from 'redux/actions/hello.actions'

function Hello(props) {
  const { helloText, sayHelloAsync, resetText } = props

  return (
    <div className="container-fluid my-3">
      <button className="btn btn-secondary btn-sm" onClick={() => sayHelloAsync('World')}>
        Say hello, after two seconds
      </button>
      <span className="mx-1" />
      <button className="btn btn-secondary btn-sm" onClick={() => resetText()}>
        Reset
      </button>
      <hr />
      {helloText?.length > 0 && (
        <h5>
          {helloText} <FontAwesomeIcon icon="hand-peace" />
        </h5>
      )}
    </div>
  )
}

Hello.propTypes = {
  helloText: PropTypes.string,
  sayHelloAsync: PropTypes.func,
  resetText: PropTypes.func,
}

export default connect(
  ({ hello }) => ({
    helloText: hello.text,
  }),
  {
    sayHelloAsync: HelloActions.sayHelloAsync,
    resetText: HelloActions.reset,
  }
)(Hello)
