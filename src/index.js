import ReactDOM from 'react-dom'
import React from 'react'
import App from 'App'
import { Provider } from 'react-redux'
import { store } from 'redux/store'
import { saga } from 'redux/middleware'
import { rootSaga } from 'redux/sagas/root.saga'
// import * as serviceWorker from './serviceWorker'

saga.run(rootSaga)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
// serviceWorker.unregister()
