import React, { Component } from 'react'
import Hello from 'components/Hello/Hello'
import 'libraries/FontAwesome'
import 'sass/_app.scss'

export default class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Hello />
      </React.Fragment>
    )
  }
}
